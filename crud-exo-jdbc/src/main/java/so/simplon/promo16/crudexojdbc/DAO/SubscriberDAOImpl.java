package so.simplon.promo16.crudexojdbc.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import so.simplon.promo16.crudexojdbc.model.Subscriber;

@Repository
public class SubscriberDAOImpl implements SubscriberDAO{

    @Autowired
    private DataSource ds;

    private static final String  GET_SUBSCRIBERS = "SELECT * FROM Subscriber";

    @Override
    public List<Subscriber> findAll() {
        
        List<Subscriber> listSub = new ArrayList<>();
        try {
            Connection cnx;
            cnx = ds.getConnection();
            PreparedStatement stm = cnx.prepareStatement(GET_SUBSCRIBERS);

            ResultSet result = stm.executeQuery();

            while(result.next()){
                listSub.add(new Subscriber(
                    result.getInt("subscriberId"),
                    result.getString("firstName"),
                    result.getString("lastName") ,
                    result.getString("email")    ,
                    result.getTimestamp("createdAt")));

            }



        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

       
        

        return null;
    }
    
}
