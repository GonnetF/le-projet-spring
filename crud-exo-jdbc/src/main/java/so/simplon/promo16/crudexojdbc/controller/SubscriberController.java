package so.simplon.promo16.crudexojdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import so.simplon.promo16.crudexojdbc.DAO.SubscriberDAO;
import so.simplon.promo16.crudexojdbc.model.Subscriber;

@Controller 
@RequestMapping("/subscribers")
public class SubscriberController  {
    @Autowired //facultatif
    SubscriberDAO subDao;


    @GetMapping("/get")
    public String getAllSubscriber(Model model){ //la classe Model fait appel a notre entité subscriber
        model.addAttribute("Subscriber", subDao.findAll());
        model.addAttribute("newsubscriber", new Subscriber());

        return "index";
    }
    
}
