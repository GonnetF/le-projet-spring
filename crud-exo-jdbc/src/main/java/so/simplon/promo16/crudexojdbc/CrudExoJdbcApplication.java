package so.simplon.promo16.crudexojdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudExoJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudExoJdbcApplication.class, args);
	}

}
