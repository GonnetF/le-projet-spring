CREATE DATABASE springboot-jdbc-thymeleaf
    DEFAULT CHARACTER SET = 'utf8mb4';

DROP TABLE I
Create Table Suscriber(
 subscriberId INTEGER AUTO_INCREMENT PRIMARY KEY,
 firstName VARCHAR(255) NOT NULL,
 lastName VARCHAR(255) NOT NULL,
 email VARCHAR(255) NOT NULL ,
 createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)

--Choisir InnoDB pour l'ENGINE, par défaut utf8 pour le charset